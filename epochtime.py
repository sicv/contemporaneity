import argparse, datetime, time

d = datetime.datetime(2017, 06, 16, 10, 00)
t = time.mktime(d.timetuple()) + d.microsecond / 1E6

print time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(t))

# 1497600000
# 2017-06-16 10:00:00

print time.gmtime(0)


